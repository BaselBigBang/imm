import 'dart:convert';

import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class AuthApi extends Api {
  final http.Client httpClient;

  AuthApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<bool> login(data) async {
    final url = '${Api.baseUrl}login';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());
    var res = jsonDecode(response.body);
    if (response.statusCode == 200) {
      var user = res['data'];
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      sharedPreferences.setInt('userRole', user['roles'][0]['id']);
      ID = user['id'];
      NAME = user['first_name'];
      TOKEN = res['data']['token'];
      EMAIL = user['email'];
      UserRole = user['roles'][0]['id'];
      ISAuth = true;
      return true;
    } else {
      return false;
    }
  }

  Future<bool> register(data) async {
    final url = '${Api.baseUrl}register';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);
    if (response.statusCode == 201) {
      var user = res['data'];
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      sharedPreferences.setInt('userRole', user['roles'][0]['id']);
      ID = user['id'];
      NAME = user['first_name'];
      EMAIL = user['email'];
      PHONE = user['phone'];
      TOKEN = res['data']['token'];
      ISAuth = true;
      return true;
    } else {
      return false;
    }
  }

  Future<List<City>> getCities() async {
    final url = '${Api.baseUrl}cities';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<City> cities = res.map((e) => City.fromJson(e)).toList();

    return cities;
  }

  Future<List<City>> getdistrict(id) async {
    final url = '${Api.baseUrl}districts/$id';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<City> cities = res.map((e) => City.fromJson(e)).toList();

    return cities;
  }
}
