import 'dart:convert';
import 'dart:io';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:http/http.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

import 'package:flutter/material.dart';

class HomeApi extends Api {
  final http.Client httpClient;

  HomeApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Place>> getCities() async {
    final url = '${Api.baseUrl}places';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Place> places = res.map((e) => Place.fromJson(e)).toList();

    return places;
  }

  Future<List<Place>> search(data) async {
    final url = '${Api.baseUrl}places/search';

    final response = await http.post(
      url,
      body: jsonEncode(data),
      headers: await getHeaders(),
    );

    var res = jsonDecode(response.body)['data'] as List;
    List<Place> places = res.map((e) => Place.fromJson(e)).toList();

    return places;
  }

  Future<List<Place>> placesUnApprove(data) async {
    final url = '${Api.baseUrl}places/search';

    final response = await http.post(
      url,
      body: jsonEncode(data),
      headers: await getHeaders(),
    );

    var res = jsonDecode(response.body)['data'] as List;
    List<Place> places = res.map((e) => Place.fromJson(e)).toList();

    return places;
  }

  Future<bool> delete(id) async {
    final url = '${Api.baseUrl}places/$id';

    final response = await http.delete(
      url,
      headers: await getHeaders(),
    );

    var res = jsonDecode(response.body)['data'];
    if (response.statusCode == 200) {
      return true;
    }

    return false;
  }

  Future<bool> updatePlace(
      Map<String, String> data, List<Asset> assets, id) async {
    String msg;
    final url = '${Api.baseUrl}places/$id';
    Uri uri = Uri.parse(url);

    MultipartRequest request = http.MultipartRequest("POST", uri);
    int cnt = 0;
    for (int i = 0; i < assets.length; i++) {
      final filePath =
          await FlutterAbsolutePath.getAbsolutePath(assets[i].identifier);

      File tempFile = await File(filePath);
      final dir = await path_provider.getTemporaryDirectory();
      final targetPath = dir.absolute.path + "/temp.jpg";

      tempFile = await FlutterImageCompress.compressAndGetFile(
        tempFile.absolute.path,
        targetPath,
        quality: 22,
      );

      final mimeTypeData =
          lookupMimeType(tempFile.path, headerBytes: [0xFF, 0xD8]).split('/');
      MultipartFile multipartFile = await http.MultipartFile.fromBytes(
        'image_path[$cnt]',
        tempFile.readAsBytesSync(),
        filename: tempFile.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
      );

      // files.add(multipartFile);
      await request.files.insert(cnt, multipartFile);
      cnt++;
    }
    request.headers.addAll(await getHeaders());
    request.fields['_method'] = data['_method'];
    request.fields['size'] = data['size'];
    request.fields['max_persons'] = data['max_persons'];
    request.fields['number_of_rooms'] = data['number_of_rooms'];
    request.fields['number_of_bathrooms'] = data['number_of_bathrooms'];
    request.fields['lat'] = data['lat'];
    request.fields['lon'] = data['lon'];
    request.fields['city_id'] = data['city_id'];
    request.fields['district_id'] = data['district_id'];
    request.fields['is_rent'] = data['is_rent'];
    request.fields['price'] = data['price'];
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    var resp = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> Add_Place(Map<String, String> data, List<Asset> assets) async {
    final url = '${Api.baseUrl}places';
    Uri uri = Uri.parse(url);

    MultipartRequest request = http.MultipartRequest("POST", uri);
    int cnt = 0;
    for (int i = 0; i < assets.length; i++) {
      final filePath =
          await FlutterAbsolutePath.getAbsolutePath(assets[i].identifier);

      File tempFile = await File(filePath);
      final dir = await path_provider.getTemporaryDirectory();
      final targetPath = dir.absolute.path + "/temp.jpg";

      tempFile = await FlutterImageCompress.compressAndGetFile(
        tempFile.absolute.path,
        targetPath,
        quality: 22,
      );

      final mimeTypeData =
          lookupMimeType(tempFile.path, headerBytes: [0xFF, 0xD8]).split('/');
      MultipartFile multipartFile = await http.MultipartFile.fromBytes(
        'image_path[$cnt]',
        tempFile.readAsBytesSync(),
        filename: tempFile.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
      );

      // files.add(multipartFile);
      await request.files.insert(cnt, multipartFile);
      cnt++;
    }
    request.headers.addAll(await getHeaders());
    request.fields['_method'] = data['_method'];
    request.fields['size'] = data['size'];
    request.fields['max_persons'] = data['max_persons'];
    request.fields['number_of_rooms'] = data['number_of_rooms'];
    request.fields['number_of_bathrooms'] = data['number_of_bathrooms'];
    request.fields['lat'] = data['lat'];
    request.fields['lon'] = data['lon'];
    request.fields['city_id'] = data['city_id'];
    request.fields['district_id'] = data['district_id'];
    request.fields['is_rent'] = data['is_rent'];
    request.fields['price'] = data['price'];
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    var resp = jsonDecode(response.body);
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> approve(data, id) async {
    final url = '${Api.baseUrl}places/change-approve-status/$id';

    final response = await http.post(
      url,
      body: jsonEncode(data),
      headers: await getHeaders(),
    );

    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
