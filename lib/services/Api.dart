import 'package:shared_preferences/shared_preferences.dart';

class Api {
  static String baseUrl = 'http://8a2731767533.ngrok.io/api/';

  Map<String, String> setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };

  Future<Map<String, String>> getHeaders() async => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${await getToken()}',
      };

  Future getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    print(localStorage.getString('token'));
    return localStorage.getString('token');
  }
}
