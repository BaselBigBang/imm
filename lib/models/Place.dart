import 'package:immpwelt/models/City.dart';

class Place {
  int id;
  double size;
  double maxPersons;
  int numberOfRooms;
  bool isRent;
  double price;
  int numberOfBathrooms;
  bool isApproved;
  City city;
  User user;
  double lat;
  double lon;
  City district;
  List images;

  Place(
      {this.id,
      this.size,
      this.maxPersons,
      this.numberOfRooms,
      this.isRent,
      this.price,
      this.numberOfBathrooms,
      this.isApproved,
      this.city,
      this.user,
      this.lat,
      this.lon,
      this.district,
      this.images});

  Place.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    size = json['size'].toDouble();
    maxPersons = json['max_persons'].toDouble();
    numberOfRooms = json['number_of_rooms'];
    isRent = json['is_rent'];
    price = json['price'].toDouble();
    ;
    numberOfBathrooms = json['number_of_bathrooms'];
    isApproved = json['is_approved'];
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    lat = json['lat'];
    lon = json['lon'];
    district =
        json['district'] != null ? new City.fromJson(json['district']) : null;
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['size'] = this.size;
    data['max_persons'] = this.maxPersons;
    data['number_of_rooms'] = this.numberOfRooms;
    data['is_rent'] = this.isRent;
    data['price'] = this.price;
    data['number_of_bathrooms'] = this.numberOfBathrooms;
    data['is_approved'] = this.isApproved;
    if (this.city != null) {
      data['city'] = this.city.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    if (this.district != null) {
      data['district'] = this.district.toJson();
    }
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class User {
  int id;
  String firstName;
  String middleName;
  String lastName;
  String phoneNumber;
  Null city;
  List<Roles> roles;
  String email;

  User(
      {this.id,
      this.firstName,
      this.middleName,
      this.lastName,
      this.phoneNumber,
      this.city,
      this.roles,
      this.email});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    phoneNumber = json['phone_number'];
    city = json['city'];
    if (json['roles'] != null) {
      roles = new List<Roles>();
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['phone_number'] = this.phoneNumber;
    data['city'] = this.city;
    if (this.roles != null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    data['email'] = this.email;
    return data;
  }
}

class Roles {
  String name;
  int id;

  Roles({this.name, this.id});

  Roles.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class Images {
  int id;
  String imagePath;

  Images({this.id, this.imagePath});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imagePath = json['image_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image_path'] = this.imagePath;
    return data;
  }
}
