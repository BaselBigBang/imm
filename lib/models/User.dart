import 'package:immpwelt/models/City.dart';

class User {
  int id;
  String firstName;
  String middleName;
  String lastName;
  String phoneNumber;
  String token;
  City city;
  List<Roles> roles;

  User(
      {this.id,
      this.firstName,
      this.middleName,
      this.lastName,
      this.phoneNumber,
      this.token,
      this.city,
      this.roles});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    phoneNumber = json['phone_number'];
    token = json['token'];
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
    if (json['roles'] != null) {
      roles = new List<Roles>();
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['phone_number'] = this.phoneNumber;
    data['token'] = this.token;
    if (this.city != null) {
      data['city'] = this.city.toJson();
    }
    if (this.roles != null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Roles {
  String name;
  int id;

  Roles({this.name, this.id});

  Roles.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}
