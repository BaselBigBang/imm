import 'dart:convert';
import 'dart:math';
import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/models/User.dart';
import 'package:immpwelt/services/AuthApi.dart';
import 'package:immpwelt/services/HomeApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeApi homeApi;

  HomeBloc({@required this.homeApi})
      : assert(homeApi != null),
        super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetHomePlaces) {
      yield HomeLoadInProgress();
      try {
        final List<Place> places = await homeApi.getCities();
        yield PlacesLoadSuccess(places: places);
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is Search) {
      yield HomeLoadInProgress();
      try {
        var data = {
          "price_from": event.price_from,
          "price_to": event.price_to,
          "max_persons_from": event.max_persons_from,
          "number_of_rooms_from": event.number_of_rooms_from,
          "number_of_rooms_to": event.number_of_rooms_to,
          "number_of_bathrooms_from": event.number_of_bathrooms_from,
          "number_of_bathrooms_to": event.number_of_bathrooms_to,
          "max_persons_to": event.max_persons_to,
          "is_rent": event.is_rent,
          "city": event.city,
          "district": event.district,
        };
        final List<Place> places = await homeApi.search(data);
        if (places.length > 0)
          yield SearchLoadSuccess(places: places);
        else
          yield SearchIsEmpty();
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is UnApprove) {
      yield HomeLoadInProgress();
      // try {
      var data = {
        "is_approved": "0",
      };
      final List<Place> places = await homeApi.placesUnApprove(data);
      if (places.length > 0)
        yield UnApproveLoadSuccess(places: places);
      else
        yield SearchIsEmpty();
      // } catch (_) {
      //   yield HomeLoadFailure();
      // }
    } else if (event is UpdatePlaces) {
      yield HomeLoadInProgress();
      try {
        Map<String, String> data = {
          '_method': 'PUT',
          'size': event.size,
          'max_persons': event.max_persons,
          'number_of_rooms': event.number_of_rooms,
          'number_of_bathrooms': event.number_of_bathrooms,
          'lat': event.lat,
          'lon': event.lon,
          'city_id': event.city_id,
          'district_id': event.district_id,
          'is_rent': event.is_rent,
          'price': event.price,
        };

        bool res = await homeApi.updatePlace(data, event.images, event.id);
        if (res) {
          yield UpdatePlaceSuccess();
        } else {
          yield UpdatePlaceSuccess();
        }
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is AddPlaces) {
      yield HomeLoadInProgress();
      try {
        Map<String, String> data = {
          '_method': 'POST',
          'size': event.size,
          'max_persons': event.max_persons,
          'number_of_rooms': event.number_of_rooms,
          'number_of_bathrooms': event.number_of_bathrooms,
          'lat': event.lat,
          'lon': event.lon,
          'city_id': event.city_id,
          'district_id': event.district_id,
          'is_rent': event.is_rent,
          'price': event.price,
        };

        bool res = await homeApi.Add_Place(data, event.images);
        if (res) {
          yield AddedPlaceSuccess();
        } else {
          yield UnaddedSuccess();
        }
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is DeleteHome) {
      yield HomeLoadInProgress();
      try {
        final bool ans = await homeApi.delete(event.id);
        yield DeleteSuccess();
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is ApproveHome) {
      yield HomeLoadInProgress();
      try {
        Map<String, String> data = {
          'status': event.status,
        };
        final bool ans = await homeApi.approve(data, event.id);
        yield ApprovedSuccess();
      } catch (_) {
        yield HomeLoadFailure();
      }
    }
  }
}
