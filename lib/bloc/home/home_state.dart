import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/models/User.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoadInProgress extends HomeState {}

class PlacesLoadSuccess extends HomeState {
  final List<Place> places;
  const PlacesLoadSuccess({
    @required this.places,
  }) : assert(places != null);

  @override
  List<Object> get props => [places];
}

class SearchLoadSuccess extends HomeState {
  final List<Place> places;
  const SearchLoadSuccess({
    @required this.places,
  }) : assert(places != null);

  @override
  List<Object> get props => [places];
}

class UnApproveLoadSuccess extends HomeState {
  final List<Place> places;
  const UnApproveLoadSuccess({
    @required this.places,
  }) : assert(places != null);

  @override
  List<Object> get props => [places];
}

class UpdatePlaceSuccess extends HomeState {
  UpdatePlaceSuccess();
  @override
  List<Object> get props => [];
}

class AddedPlaceSuccess extends HomeState {
  AddedPlaceSuccess();
  @override
  List<Object> get props => [];
}

class UnaddedSuccess extends HomeState {}

class UnUpdatePlaceSuccess extends HomeState {}

class SearchIsEmpty extends HomeState {}

class DeleteSuccess extends HomeState {}

class ApprovedSuccess extends HomeState {}

class HomeLoadFailure extends HomeState {}
