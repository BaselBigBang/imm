import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class HomeRequested extends HomeEvent {
  const HomeRequested();

  @override
  List<Object> get props => [];
}

class GetHomePlaces extends HomeEvent {
  @override
  List<Object> get props => [];
}

class Search extends HomeEvent {
  final String price_from;
  final String price_to;
  final String max_persons_from;
  final String number_of_rooms_from;
  final String number_of_rooms_to;
  final String number_of_bathrooms_from;
  final String number_of_bathrooms_to;
  final String max_persons_to;
  final String is_rent;
  final String city;
  final String district;

  const Search({
    this.price_from,
    this.price_to,
    this.max_persons_from,
    this.number_of_rooms_from,
    this.number_of_rooms_to,
    this.number_of_bathrooms_from,
    this.number_of_bathrooms_to,
    this.max_persons_to,
    this.is_rent,
    this.city,
    this.district,
  });
  @override
  List<Object> get props => [];
}

class UnApprove extends HomeEvent {
  final String is_approved;

  const UnApprove({
    this.is_approved,
  });
  @override
  List<Object> get props => [];
}

class DeleteHome extends HomeEvent {
  final int id;
  const DeleteHome({
    this.id,
  });
  @override
  List<Object> get props => [];
}

class ApproveHome extends HomeEvent {
  final int id;
  final String status;
  const ApproveHome({this.id, this.status});
  @override
  List<Object> get props => [];
}

class UpdatePlaces extends HomeEvent {
  final String size;
  final String max_persons;
  final String number_of_rooms;
  final String number_of_bathrooms;
  final String lat;
  final String lon;
  final String city_id;
  final String district_id;
  final String is_rent;
  final String price;
  final List<Asset> images;
  final int id;
  UpdatePlaces(
      {this.size,
      this.max_persons,
      this.number_of_rooms,
      this.number_of_bathrooms,
      this.lat,
      this.lon,
      this.city_id,
      this.district_id,
      this.is_rent,
      this.price,
      this.images,
      this.id});

  @override
  List<Object> get props => [];
}

class AddPlaces extends HomeEvent {
  final String size;
  final String max_persons;
  final String number_of_rooms;
  final String number_of_bathrooms;
  final String lat;
  final String lon;
  final String city_id;
  final String district_id;
  final String is_rent;
  final String price;
  final List<Asset> images;
  final int id;
  AddPlaces(
      {this.size,
      this.max_persons,
      this.number_of_rooms,
      this.number_of_bathrooms,
      this.lat,
      this.lon,
      this.city_id,
      this.district_id,
      this.is_rent,
      this.price,
      this.images,
      this.id});

  @override
  List<Object> get props => [];
}
