import 'dart:convert';
import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/User.dart';
import 'package:immpwelt/services/AuthApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthApi authApi;

  AuthBloc({@required this.authApi})
      : assert(authApi != null),
        super(AuthInitial());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthRequested) {
      yield AuthLoadInProgress();
      try {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        bool isAuth = false;
        if (sharedPreferences.containsKey('isAuth') &&
            sharedPreferences.containsKey('token')) {
          isAuth = await sharedPreferences.getBool('isAuth');
          ID = jsonDecode(await sharedPreferences.getString('user'))['id'];
          NAME = jsonDecode(
              await sharedPreferences.getString('user'))['first_name'];
          TOKEN = await sharedPreferences.getString('token');
          UserRole = await sharedPreferences.getInt('userRole');
          ISAuth = isAuth;
        }

        if (isAuth) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is LoginRequested) {
      yield AuthLoadInProgress();
      // try {
      var data = {
        'email': event.email,
        'phone_number': event.phone,
        'password': event.password,
      };
      bool ans = await authApi.login(data);
      if (ans) {
        yield Authenticated();
      } else {
        yield NotAuthenticated();
      }
      // } catch (_) {
      //   yield AuthLoadFailure();
      // }
    } else if (event is RegisterRequested) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'email': event.email,
          'phone_number': event.phone,
          'first_name': event.fname,
          'middle_name': event.mname,
          'last_name': event.lname,
          'city_id': event.city_id,
          'password': event.password,
        };
        bool ans = await authApi.register(data);
        if (ans) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is LogoutRequested) {
      yield AuthLoadInProgress();
      try {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.remove('token');
        sharedPreferences.setBool('isAuth', false);
        sharedPreferences.remove('user');
        sharedPreferences.remove('userRole');
        UserRole = 0;
        bool ans = false;
        ISAuth = false;

        if (ans) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is GetCities) {
      try {
        List<City> cities = await authApi.getCities();
        yield CitiesLoadSuccess(cities: cities);
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is Getdistrict) {
      try {
        List<City> cities = await authApi.getdistrict(event.id);
        yield DistrictsLoadSuccess(cities: cities);
      } catch (_) {
        yield AuthLoadFailure();
      }
    }
  }
}
