import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/User.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoadInProgress extends AuthState {}

class Authenticated extends AuthState {}

class NotAuthenticated extends AuthState {}

class AuthLoadFailure extends AuthState {}

class CitiesLoadSuccess extends AuthState {
  final List<City> cities;
  const CitiesLoadSuccess({
    @required this.cities,
  }) : assert(cities != null);

  @override
  List<Object> get props => [cities];
}

class DistrictsLoadSuccess extends AuthState {
  final List<City> cities;
  const DistrictsLoadSuccess({
    @required this.cities,
  }) : assert(cities != null);

  @override
  List<Object> get props => [cities];
}
