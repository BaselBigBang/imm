import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class AuthRequested extends AuthEvent {
  const AuthRequested();

  @override
  List<Object> get props => [];
}

class LoginRequested extends AuthEvent {
  final String email;
  final String phone;
  final String password;
  const LoginRequested({this.email, this.phone, this.password});

  @override
  List<Object> get props => [];
}

class RegisterRequested extends AuthEvent {
  final String email;
  final String password;
  final String fname;
  final String lname;
  final String mname;
  final String phone;
  final int city_id;
  const RegisterRequested(
      {this.email,
      this.fname,
      this.mname,
      this.lname,
      this.phone,
      this.password,
      this.city_id});

  @override
  List<Object> get props => [];
}

class GetCities extends AuthEvent {
  @override
  List<Object> get props => [];
}

class LogoutRequested extends AuthEvent {
  @override
  List<Object> get props => [];
}

class Getdistrict extends AuthEvent {
  final int id;
  const Getdistrict({
    this.id,
  });
  @override
  List<Object> get props => [];
}
