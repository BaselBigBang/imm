import 'package:flutter/material.dart';
import 'package:immpwelt/config.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PlaceWidget extends StatelessWidget {
  final Place place;
  const PlaceWidget({Key key, this.place}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: Colors.grey.shade100,
        boxShadow: [
          BoxShadow(color: Colors.black),
        ],
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: place.images.isEmpty
                      ? Image.asset(
                          'assets/real_Estate.jpg',
                          fit: BoxFit.cover,
                        )
                      :
                      //  place.images[0].imagePath == null

                      CachedNetworkImage(
                          imageUrl:
                              Config.baseImaage + place.images[0].imagePath,
                          placeholder: (context, url) =>
                              Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          fit: BoxFit.cover,
                        )
//              color: Colors.grey[200],
                  // :
//                     CachedNetworkImage(
//                   imageUrl: place.images[0].imagePath,
//                   placeholder: (context, url) =>
//                       Center(child: CircularProgressIndicator()),
//                   errorWidget: (context, url, error) => Icon(Icons.error),
//                   fit: BoxFit.fill,
// //              color: Colors.grey[200],
//                 ),
                  ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                place.city.displayName,
                style: TextStyle(color: Colors.black, fontSize: 16),
              ),
            ),
          ]),
    );
  }
}
