import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:immpwelt/config.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class ListAddImages extends StatelessWidget {
  final List<Asset> names;
  final Function func;
  final int index;
  const ListAddImages({Key key, this.names, this.func, this.index})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: 100,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(names.length, (index) {
            Asset asset = names[index];
            return Stack(
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Config.primaryColor,
                      ),
                      borderRadius: BorderRadius.circular(10)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: AssetThumb(
                      asset: asset,
                      width: 320,
                      height: 330,
                    ),
                  ),
                ),
                Positioned(
                    top: 0,
                    right: 0,
                    child: GestureDetector(
                      onTap: () {
                        func(index);
                      },
                      child: Icon(
                        Icons.cancel_outlined,
                        color: Config.primaryColor,
                        size: 30,
                      ),
                    )),
              ],
            );
          }),
        ),
      ),
    );
  }
}
