import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:immpwelt/config.dart';

Widget myAppBar(title, actions) {
  return AppBar(
      title: Text(title),
      actions: actions,
      shape: continuousRectangleBorder,
      centerTitle: true,
      //backgroundColor: Config.primaryColor,
      flexibleSpace: Container(
        decoration: BoxDecoration(color: Config.primaryColor),
      ));
}

ContinuousRectangleBorder continuousRectangleBorder =
    ContinuousRectangleBorder();
