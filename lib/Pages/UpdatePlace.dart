import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:immpwelt/Pages/AllPlacesPage.dart';
import 'package:immpwelt/Pages/SelectLocation.dart';
import 'package:immpwelt/bloc/auth/auth_bloc.dart';
import 'package:immpwelt/bloc/auth/auth_event.dart';
import 'package:immpwelt/bloc/auth/auth_state.dart';
import 'package:immpwelt/bloc/home/home_bloc.dart';
import 'package:immpwelt/bloc/home/home_event.dart';
import 'package:immpwelt/bloc/home/home_state.dart';
import 'package:immpwelt/config.dart';
import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/models/User.dart';
import 'package:immpwelt/services/AuthApi.dart';
import 'package:immpwelt/services/HomeApi.dart';
import 'package:immpwelt/widgets/GridViewImage.dart';
import 'package:immpwelt/widgets/MyAppBar.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';

import 'LoginPage.dart';

class UpdatePlace extends StatefulWidget {
  const UpdatePlace({Key key}) : super(key: key);

  @override
  _UpdatePlaceState createState() => _UpdatePlaceState();
}

class _UpdatePlaceState extends State<UpdatePlace> {
  List<String> cities = ["damascus", "Homs"];
  List<City> cities2 = [];
  List<City> district = [];
  List<Place> pleaces = [];
  bool isRent = true;
  bool isPruches = false;
  int cityId;
  int districtId = 1;
  int selectedCity;
  int selecteddistrict;
  final TextEditingController city = TextEditingController();
  final TextEditingController areato = TextEditingController();
  final TextEditingController numberofpeoplet = TextEditingController();
  final TextEditingController numberofroomst = TextEditingController();
  final TextEditingController numberobathfroomst = TextEditingController();
  final TextEditingController pricef = TextEditingController();
  final TextEditingController pricet = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<Asset> images = [];
  String _error = 'No Error Dectected';
  AuthBloc authBloc;
  AuthApi authApi = AuthApi(httpClient: http.Client());
  String area;
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  double longitude, latitude;
  bool selectlocation = false;
  bool isEdited = false;
  String is_Rent = "1";
  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#fcc422",
          actionBarTitle: "Add Pictures",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    homeBloc = HomeBloc(homeApi: homeApi);
    authBloc.add(GetCities());
  }

  void _delete(int index) {
    images.removeAt(index);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    Map args = ModalRoute.of(context).settings.arguments;
    final Place place = args['pleace'];
    return Scaffold(
      appBar: myAppBar("Update Home", null),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          isRent = true;
                          isPruches = false;
                          is_Rent = "1";
                        });
                      },
                      child: Container(
                        width: size.width,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: isRent ? Config.primaryColor : Colors.white,
                            border: Border.all(
                                width: 1,
                                color: isRent
                                    ? Config.primaryColor
                                    : Colors.grey)),
                        child: Center(child: Text("Rent")),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          isRent = false;
                          isPruches = true;
                          is_Rent = "0";
                        });
                      },
                      child: Container(
                        width: size.width,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color:
                                isPruches ? Config.primaryColor : Colors.white,
                            border: Border.all(
                                width: 1,
                                color: isPruches
                                    ? Config.primaryColor
                                    : Colors.grey)),
                        child: Center(child: Text("Purchase")),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              BlocListener(
                cubit: homeBloc,
                listener: (context, state) {
                  if (state is UpdatePlaceSuccess) {
                    Get.snackbar('Success', 'Place Updated Successfully',
                        snackPosition: SnackPosition.TOP,
                        overlayBlur: 3,
                        dismissDirection: SnackDismissDirection.HORIZONTAL,
                        colorText: Config.primaryColor,
                        backgroundColor: Colors.black,
                        duration: Duration(seconds: 3));
                  }
                },
                child: BlocBuilder(
                  cubit: homeBloc,
                  builder: (context, state) {
                    if (state is HomeLoadInProgress) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    if (state is HomeInitial ||
                        state is SearchLoadSuccess ||
                        state is HomeLoadFailure ||
                        state is UpdatePlaceSuccess ||
                        state is UnUpdatePlaceSuccess) {
                      if (!isEdited) {
                        areato.text = place.size.toString();

                        numberofpeoplet.text = place.maxPersons.toString();

                        numberofroomst.text = place.numberOfRooms.toString();

                        numberobathfroomst.text =
                            place.numberOfBathrooms.toString();
                        pricet.text = place.price.toString();

                        isEdited = true;
                      }
                      return Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Home Space",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: areato,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\m2",
                                              textAlign: TextAlign.right,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'Max',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of People",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofpeoplet,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.person,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'Max',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of Rooms",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofroomst,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bed,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'Max',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of BathRoom",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberobathfroomst,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bathroom,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'Max',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Price",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: pricet,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\$",
                                              textAlign: TextAlign.right,
                                              textDirection: TextDirection.rtl,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'Max',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: GestureDetector(
                                  onTap: () async {
                                    Map res = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              SelectLocation(),
                                        ));
                                    print(res);
                                    setState(() {
                                      latitude = res['latlang'].latitude;
                                      longitude = res['latlang'].longitude;
                                      selectlocation = !selectlocation;
                                    });
                                    print(latitude);
                                  },
                                  child: Container(
                                    width: 50,
                                    height: 60,
                                    child: Stack(
                                      children: [
                                        Align(
                                            alignment: Alignment.center,
                                            child: Text("location")),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 4),
                                          child: Container(
                                            width: 50,
                                            height: 50,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(45)),
                                              color: Config.primaryColor,
                                            ),
                                            child: Icon(
                                              Icons.location_pin,
                                              color: Colors.white,
                                              size: 30,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(45)),
                                      border: Border.all(color: Colors.black38),
                                    ),
                                  ),
                                )),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: size.width,
                              height: size.height * 0.07,
                              child: RaisedButton(
                                  color: Config.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(200.0),
                                  ),
                                  autofocus: false,
                                  textColor: Colors.white,
                                  child: Icon(
                                    Icons.camera_alt_outlined,
                                    size: 35,
                                  ),
                                  onPressed: () {
                                    loadAssets();
                                  }),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ListAddImages(
                              names: images,
                              func: _delete,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            GestureDetector(
                              onTap: () {
                                if (areato.text.length == 0) {
                                  Get.snackbar('Error', 'Please Enter the Area',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }

                                if (numberofpeoplet.text.length == 0) {
                                  Get.snackbar('Error',
                                      'Please Enter the Number of Person',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (pricet.text.length == 0) {
                                  Get.snackbar(
                                      'Error', 'Please Enter the Price',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (pricet.text.length == 0) {
                                  Get.snackbar(
                                      'Error', 'Please Enter the Price',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                homeBloc.add(UpdatePlaces(
                                    price: areato.text,
                                    number_of_rooms: numberofroomst.text,
                                    number_of_bathrooms:
                                        numberobathfroomst.text,
                                    max_persons: numberofpeoplet.text,
                                    is_rent: is_Rent,
                                    city_id: place.city.id.toString(),
                                    district_id: place.district.id.toString(),
                                    lat: latitude.toString(),
                                    lon: longitude.toString(),
                                    size: areato.text,
                                    images: images,
                                    id: place.id));
                                // Navigator.pushReplacementNamed(context, '/test');
                              },
                              child: Container(
                                width: size.width * 0.85,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  color: Config.primaryColor,
                                  borderRadius: BorderRadius.horizontal(
                                    left: Radius.circular(10),
                                    right: Radius.circular(10),
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Update",
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
