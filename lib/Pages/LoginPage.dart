import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/snackbar/snack.dart';
import 'package:immpwelt/Pages/HomePage.dart';
import 'package:immpwelt/bloc/auth/auth_bloc.dart';
import 'package:immpwelt/bloc/auth/auth_event.dart';
import 'package:immpwelt/bloc/auth/auth_state.dart';
import 'package:immpwelt/bloc/home/home_state.dart';

import '../../constants.dart';
import '../config.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController phone = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLogin = false;
  bool isVis = true;
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            flexibleSpace: Container(
          decoration: BoxDecoration(),
        )),
        body: BlocListener(
          cubit: authBloc,
          listener: (context, state) {
            if (state is NotAuthenticated) {
              Get.snackbar('', 'Please Check your Email or Password',
                  snackPosition: SnackPosition.TOP,
                  overlayBlur: 3,
                  dismissDirection: SnackDismissDirection.HORIZONTAL,
                  colorText: Config.primaryColor,
                  backgroundColor: Colors.black,
                  duration: Duration(seconds: 1));
            }
            if (state is Authenticated) {
              Navigator.pushReplacementNamed(context, '/home');
            }
          },
          child: BlocBuilder(
            cubit: authBloc,
            builder: (context, state) {
              if (state is AuthInitial ||
                  state is NotAuthenticated ||
                  state is Authenticated ||
                  state is CitiesLoadSuccess ||
                  state is SearchLoadSuccess ||
                  state is DistrictsLoadSuccess) {
                return SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: sizeAware.height * 0.15,
                          ),
                          Text(
                            "Welcome",
                            style: TextStyle(
                                color: Config.primaryColor,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: sizeAware.height * 0.01,
                          ),
                          Text(
                            "Sign in to continue",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: sizeAware.height * 0.01,
                          ),
                          TextFormField(
                            controller: emailTextEditingController,
                            decoration: InputDecoration(
                              labelText: 'Email',
                            ),
                            validator: (String arg) {
                              if (arg.length <= 0)
                                return "Email is required";
                              else
                                return null;
                            },
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            controller: phone,
                            decoration: InputDecoration(
                              labelText: 'Phone',
                            ),
                            validator: (String arg) {
                              if (arg.length <= 0)
                                return "Phone is required";
                              else
                                return null;
                            },
                          ),
                          TextFormField(
                            controller: passwordTextEditingController,
                            obscureText: isVis,
                            validator: (String arg) {
                              if (arg.length <= 0)
                                return "Password is required";
                              else
                                return null;
                            },
                            decoration: InputDecoration(
                              labelText: "Password",
                              suffixIcon: IconButton(
                                icon: isVis
                                    ? Icon(Icons.visibility_off)
                                    : Icon(Icons.remove_red_eye),
                                onPressed: () {
                                  setState(() {
                                    isVis = !isVis;
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: sizeAware.height * 0.03,
                          ),
                          Container(
                            width: sizeAware.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    if (_formKey.currentState.validate()) {
                                      authBloc.add(LoginRequested(
                                        email: emailTextEditingController.text
                                            .trim(),
                                        phone: phone.text.trim(),
                                        password: passwordTextEditingController
                                            .text
                                            .trim(),
                                      ));
                                    }
                                  },
                                  child: Container(
                                    width: sizeAware.width * 0.85,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      color: Config.primaryColor,
                                      borderRadius: BorderRadius.horizontal(
                                        left: Radius.circular(40),
                                        right: Radius.circular(40),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Login",
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                // facebook and google
                                /*Container(
                                  width: sizeAware.width * 0.85,
                                  height: 50.0,
                                  child: FacebookAuthButton(
                                    onPressed:
                                        _userData != null ? _logOut : _login,
                                    borderRadius: 40,
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  width: sizeAware.width * 0.85,
                                  height: 50.0,
                                  child: GoogleAuthButton(
                                    onPressed: () async {
                                      googleSignIn();
                                    },
                                    borderRadius: 40,
                                  ),
                                ),*/

                                // Text(
                                //   getTranslated(context,'or'),
                                //   style: TextStyle(
                                //     fontSize: 18,
                                //     fontWeight: FontWeight.bold,
                                //     color: Colors.grey[600],
                                //   ),
                                // ),
                                // SizedBox(
                                //   height: sizeAware.height * 0.02,
                                // ),
                                // Container(
                                //   width: sizeAware.width * 0.85,
                                //   height: 50.0,
                                //   decoration: BoxDecoration(
                                //     border: Border.all(),
                                //     borderRadius: BorderRadius.horizontal(
                                //       left: Radius.circular(40),
                                //       right: Radius.circular(40),
                                //     ),
                                //   ),
                                //   child: Padding(
                                //     padding: const EdgeInsets.all(8.0),
                                //     child: Row(
                                //       mainAxisAlignment: MainAxisAlignment.center,
                                //       children: [
                                //         Text(
                                //          getTranslated(context, 'Login with Facebook'),
                                //           style: TextStyle(
                                //             fontSize: 17,
                                //             color: Colors.grey[600],
                                //             fontWeight: FontWeight.bold,
                                //           ),
                                //         ),
                                //       ],
                                //     ),
                                //   ),
                                // ),
                                // SizedBox(
                                //   height: sizeAware.height * 0.03,
                                // ),

                                GestureDetector(
                                  onTap: () {
                                    // Navigator.pushReplacementNamed(
                                    //     context, '/register');
                                    Navigator.pushReplacementNamed(
                                        context, '/register');
                                  },
                                  child: Container(
                                    width: sizeAware.width,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Dont have an account?',
                                          style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[600],
                                          ),
                                        ),
                                        Text(
                                          "Sign Up",
                                          style: TextStyle(
                                            fontSize: 17,
                                            color: Config.primaryColor,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: sizeAware.height * 0.2,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
              if (state is AuthLoadInProgress) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is AuthLoadFailure) {
                return Scaffold(
                  body: SafeArea(
                    child: Container(
                      width: sizeAware.width,
                      height: sizeAware.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Connection Error',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 20),
                          FlatButton(
                            color: Config.primaryColor,
                            onPressed: () {
                              setState(() {
                                authBloc.add(AuthRequested());
                              });
                            },
                            child: Text(
                              "Refresh",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ));
  }
}
