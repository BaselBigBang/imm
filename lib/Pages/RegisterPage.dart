import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import 'package:http/http.dart' as http;
import 'package:immpwelt/bloc/auth/auth_bloc.dart';
import 'package:immpwelt/bloc/auth/auth_event.dart';
import 'package:immpwelt/bloc/auth/auth_state.dart';
import 'package:immpwelt/config.dart';
import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/User.dart';
import 'package:immpwelt/services/AuthApi.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool isVis = true;
  AuthBloc authBloc;
  AuthApi authApi = AuthApi(httpClient: http.Client());
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController firstNameTextEditingController =
      TextEditingController();
  TextEditingController middleTextEditingController = TextEditingController();
  TextEditingController lastNameTextEditingController = TextEditingController();
  TextEditingController phoneTextEditingController = TextEditingController();

  TextEditingController numberTextEditingController = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      TextEditingController();
  TextEditingController city = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<City> cities = [];

  String countryCode = "+971";

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    authBloc.add(GetCities());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
      ),
      backgroundColor: Colors.white,
      body: BlocListener(
        cubit: authBloc,
        listener: (context, state) {
          if (state is NotAuthenticated) {
            Get.snackbar('Attention', 'Please Try again later',
                snackPosition: SnackPosition.TOP,
                overlayBlur: 3,
                dismissDirection: SnackDismissDirection.HORIZONTAL,
                colorText: Config.primaryColor,
                backgroundColor: Colors.black,
                duration: Duration(seconds: 3));
          }
          if (state is Authenticated) {
            Navigator.pushReplacementNamed(context, '/home');
          }
        },
        child: BlocBuilder(
          cubit: authBloc,
          builder: (context, state) {
            if (state is AuthInitial ||
                state is Authenticated ||
                state is NotAuthenticated ||
                state is CitiesLoadSuccess) {
              if (state is CitiesLoadSuccess) {
                cities = state.cities;
              }
              return SingleChildScrollView(
                child: Container(
                  width: sizeAware.width,
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: sizeAware.height * 0.03,
                        ),
                        Text(
                          "Register",
                          style: TextStyle(
                              color: Config.primaryColor,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        TextFormField(
                          controller: firstNameTextEditingController,
                          decoration: InputDecoration(
                            labelText: "First Name",
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "First Name is required";
                            else
                              return null;
                          },
                        ),
                        TextFormField(
                          controller: middleTextEditingController,
                          decoration: InputDecoration(
                            labelText: "Middle Name",
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Middle Name is required";
                            else
                              return null;
                          },
                        ),
                        TextFormField(
                          controller: lastNameTextEditingController,
                          decoration: InputDecoration(
                            labelText: "Last Name",
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Last Name is required";
                            else
                              return null;
                          },
                        ),
                        TextFormField(
                          controller: emailTextEditingController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: "Email",
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Email is required";
                            else
                              return null;
                          },
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        TextFormField(
                          controller: phoneTextEditingController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: "Phone",
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Phone is required";
                            else
                              return null;
                          },
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.02,
                        ),
                        Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            border: Border.all(),
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8,
                              ),
                              child: DropdownButton(
                                value: city.text == ''
                                    ? null
                                    : int.parse(city.text),
                                hint: Text(
                                  "City",
                                ),
                                underline: SizedBox(),
                                isExpanded: true,
                                onChanged: (val) {
                                  setState(() {
                                    city.text = val.toString();
                                  });
                                  print(city.text);
                                },
                                items: cities
                                    .map(
                                      (e) => DropdownMenuItem<int>(
                                        value: e.id,
                                        child: Text(
                                          e.displayName,
                                          style: TextStyle(),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        TextFormField(
                          obscureText: isVis,
                          controller: passwordTextEditingController,
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Password is required";
                            else
                              return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Password",
                            suffixIcon: IconButton(
                              icon: isVis
                                  ? Icon(Icons.visibility_off)
                                  : Icon(Icons.remove_red_eye),
                              onPressed: () {
                                setState(() {
                                  isVis = !isVis;
                                });
                              },
                            ),
                          ),
                        ),
                        TextFormField(
                          obscureText: isVis,
                          controller: confirmPasswordTextEditingController,
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return "Password Confirmation is required";
                            if (arg != passwordTextEditingController.text)
                              return "Password confirmation does not match";
                            else
                              return null;
                          },
                          decoration: InputDecoration(
                            labelText: "Confirm Password",
                            suffixIcon: IconButton(
                              icon: isVis
                                  ? Icon(Icons.visibility_off)
                                  : Icon(Icons.remove_red_eye),
                              onPressed: () {
                                setState(() {
                                  isVis = !isVis;
                                });
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.03,
                        ),
                        Container(
                          width: sizeAware.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  if (city.text == '') {
                                    Get.snackbar(
                                        'Attention', 'Please Enter the Price',
                                        snackPosition: SnackPosition.BOTTOM,
                                        overlayBlur: 3,
                                        dismissDirection:
                                            SnackDismissDirection.HORIZONTAL,
                                        colorText: Config.primaryColor,
                                        backgroundColor: Colors.black,
                                        duration: Duration(seconds: 3));
                                  }
                                  if (_formKey.currentState.validate() &&
                                      city.text != '') {
                                    if (numberTextEditingController.text
                                        .startsWith('0')) {
                                      numberTextEditingController.text =
                                          numberTextEditingController.text
                                              .substring(1);
                                    }
                                    authBloc.add(RegisterRequested(
                                      email: emailTextEditingController.text
                                          .trim(),
                                      password: passwordTextEditingController
                                          .text
                                          .trim(),
                                      fname: firstNameTextEditingController.text
                                          .trim(),
                                      lname: lastNameTextEditingController.text
                                          .trim(),
                                      mname: middleTextEditingController.text
                                          .trim(),
                                      phone: phoneTextEditingController.text
                                          .trim(),
                                      city_id: int.parse(city.text),
                                    ));
                                  }
                                },
                                child: Container(
                                  width: sizeAware.width * 0.85,
                                  height: 50.0,
                                  decoration: BoxDecoration(
                                    color: Config.primaryColor,
                                    borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(40),
                                      right: Radius.circular(40),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Register",
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: sizeAware.height * 0.03,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacementNamed(
                                      context, '/login');
                                },
                                child: Container(
                                  width: sizeAware.width,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Already have an account?'",
                                        style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey[600],
                                        ),
                                      ),
                                      Text(
                                        "Login",
                                        style: TextStyle(
                                          fontSize: 17,
                                          color:
                                              Color.fromRGBO(53, 9, 100, 1.0),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.1,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is AuthLoadInProgress) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is AuthLoadFailure) {
              return Scaffold(
                body: SafeArea(
                  child: Container(
                    width: sizeAware.width,
                    height: sizeAware.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Connection Error',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 20),
                        FlatButton(
                          color: Config.primaryColor,
                          onPressed: () {
                            setState(() {
                              authBloc.add(AuthRequested());
                            });
                          },
                          child: Text(
                            'Refresh',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
