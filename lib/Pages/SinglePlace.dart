import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:immpwelt/bloc/home/home_bloc.dart';
import 'package:immpwelt/bloc/home/home_event.dart';
import 'package:immpwelt/bloc/home/home_state.dart';
import 'package:immpwelt/config.dart';
import 'package:immpwelt/constants.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/services/HomeApi.dart';
import 'package:immpwelt/widgets/ImageSlider.dart';
import 'package:immpwelt/widgets/MyAppBar.dart';
import 'package:http/http.dart' as http;

class SinglePlace extends StatefulWidget {
  const SinglePlace({Key key}) : super(key: key);

  @override
  _SinglePlaceState createState() => _SinglePlaceState();
}

class _SinglePlaceState extends State<SinglePlace> {
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    Map args = ModalRoute.of(context).settings.arguments;
    final Place place = args['pleace'];
    return Scaffold(
      appBar: myAppBar("The Place", null),
      body: Container(
        width: size.width,
        height: size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Container(
              //   height: 170,
              //   color: Colors.red,
              // )
              Container(
                height: 170,
                child: ImageSlider(
                  images: place.images,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "Home Information",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey.shade800),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Size:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.size.toString() + "m2",
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Max Persons:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.maxPersons.toString(),
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Number of Rooms:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.numberOfRooms.toString(),
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Number of Bathrooms:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.numberOfBathrooms.toString(),
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "City:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.city.displayName,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Price:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.price.toString() + " SP",
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              UserRole == 1
                  ? Padding(
                      padding: const EdgeInsets.only(left: 10, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Status:",
                            style: TextStyle(
                                fontSize: 18, color: Config.primaryColor),
                          ),
                          Spacer(),
                          BlocListener(
                            cubit: homeBloc,
                            listener: (context, state) {
                              if (state is ApprovedSuccess) {
                                Navigator.pushReplacementNamed(
                                    context, '/home');
                                Get.snackbar(
                                    'Success', 'Place Approved Successfully',
                                    snackPosition: SnackPosition.BOTTOM,
                                    overlayBlur: 3,
                                    dismissDirection:
                                        SnackDismissDirection.HORIZONTAL,
                                    colorText: Config.primaryColor,
                                    backgroundColor: Colors.black,
                                    duration: Duration(seconds: 3));
                              }
                            },
                            child: BlocBuilder(
                              cubit: homeBloc,
                              builder: (context, state) {
                                if (state is HomeInitial) {
                                  return ElevatedButton(
                                      onPressed: () {
                                        homeBloc.add(ApproveHome(
                                            status:
                                                place.isApproved ? "0" : "1",
                                            id: place.id));
                                      },
                                      child: place.isApproved
                                          ? Text("Un Approve")
                                          : Text("Approve"));
                                }
                                return Container();
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "Owner Information",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey.shade800),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "First Name:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.user.firstName,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Middle Name:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.user.middleName,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Last Name:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.user.lastName,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Phone:",
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                    ),
                    Spacer(),
                    Text(
                      place.user.phoneNumber,
                      style: TextStyle(fontSize: 18, color: Colors.black),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UserRole == 1 || UserRole == place.user.id
                        ? Container(
                            child: ElevatedButton(
                              child: Text("Update"),
                              onPressed: () {
                                Navigator.pushNamed(context, '/update',
                                    arguments: {
                                      'pleace': place,
                                    });
                              },
                            ),
                          )
                        : SizedBox(),
                    Spacer(),
                    UserRole == 1 || UserRole == place.user.id
                        ? Container(
                            child: BlocListener(
                              cubit: homeBloc,
                              listener: (context, state) {
                                if (state is DeleteSuccess) {
                                  Navigator.pushReplacementNamed(
                                      context, '/home');
                                  Get.snackbar(
                                      'Success', 'Home Deleted Successfully',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                }
                              },
                              child: BlocBuilder(
                                cubit: homeBloc,
                                builder: (context, state) {
                                  if (state is HomeInitial) {
                                    return ElevatedButton(
                                      child: Text(state is HomeLoadInProgress
                                          ? CircularProgressIndicator()
                                          : "Delete"),
                                      onPressed: () {
                                        homeBloc.add(DeleteHome(id: place.id));
                                      },
                                    );
                                  }
                                  return Container();
                                },
                              ),
                            ),
                          )
                        : SizedBox(),
                    Spacer(),
                    Container(
                      child: ElevatedButton(
                        child: Text("Go To Map"),
                        onPressed: () {
                          Navigator.pushNamed(context, '/map', arguments: {
                            'pleace': place,
                          });
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
