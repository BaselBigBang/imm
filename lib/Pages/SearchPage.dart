import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:immpwelt/bloc/home/home_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:immpwelt/bloc/home/home_event.dart';
import 'package:immpwelt/bloc/home/home_state.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/services/HomeApi.dart';
import 'package:immpwelt/widgets/PlaceWidget.dart';

class SearchResult extends StatefulWidget {
  const SearchResult({Key key}) : super(key: key);

  @override
  _SearchResultState createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  List<Place> places;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments;
    final List<Place> places = args['pleaces'];
    return Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.count(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                childAspectRatio: 0.9,
                children: places
                    .map((e) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/SinglePlace',
                                  arguments: {
                                    'pleace': e,
                                  });
                            },
                            child: PlaceWidget(
                              place: e,
                            ),
                          ),
                        ))
                    .toList()),
          ),
        ));
  }
}
