import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:immpwelt/models/Place.dart';

class MyMap extends StatefulWidget {
  const MyMap({Key key}) : super(key: key);

  @override
  _MyMapState createState() => _MyMapState();
}

class _MyMapState extends State<MyMap> {
  Set<Marker> markers = Set<Marker>();
  Place place;
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  double lat;
  double lng;
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(33.510414, 36.278336),
    zoom: 14.4746,
  );

  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments;
    final Place place = args['pleace'];
    lat = place.lat;
    lng = place.lon;
    return new Scaffold(
      body: GoogleMap(
        markers: _getMarkers(place),
        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: Align(
        alignment: Alignment.bottomCenter,
        child: FloatingActionButton.extended(
          onPressed: () async {
            CameraPosition _kLake = CameraPosition(
              bearing: 192.8334901395799,
              target: LatLng(lat, lng),
            );
            final GoogleMapController controller = await _controller.future;
            controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
          },
          label: Text('Get the Location'),
        ),
      ),
    );
  }

  // static final CameraPosition _kLake = CameraPosition(
  //   bearing: 192.8334901395799,
  //   target: LatLng(33.510414, 36.278336),
  // );
  // Future<void> _goToTheLake() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  // }

  _getMarkers(Place place) {
    Set<Marker> ms = Set<Marker>();
    PointObject point2 = PointObject(
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(place.city.displayName ?? ''),

              Icon(Icons.arrow_forward_ios_outlined),

              // Container(
              //   width: 50,
              //   height: 60,
              //   decoration: BoxDecoration(
              //     shape: BoxShape.rectangle,
              //     image: DecorationImage(
              //       fit: BoxFit.cover,
              //       image: AssetImage("assets/hourse.jpg"),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
      location: LatLng(place.lat, place.lon),
    );

    ms.add(
      Marker(
        markerId: MarkerId(point2.location.latitude.toString() +
            point2.location.longitude.toString()),
        position: point2.location,
        icon: BitmapDescriptor.defaultMarker,
      ),
    );

    return ms;
  }

  _onTap(PointObject point) async {
    final RenderBox renderBox = context.findRenderObject();
    Rect _itemRect = renderBox.localToGlobal(Offset.zero) & renderBox.size;

    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude - 0.0001,
            point.location.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude,
            point.location.longitude,
          ),
          zoom: 10,
        ),
      ),
    );
  }
}

class PointObject {
  final Widget child;
  final LatLng location;

  PointObject({this.child, this.location});
}
