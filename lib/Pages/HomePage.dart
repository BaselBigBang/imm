import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:immpwelt/Pages/AllPlacesPage.dart';
import 'package:immpwelt/bloc/auth/auth_bloc.dart';
import 'package:immpwelt/bloc/auth/auth_event.dart';
import 'package:immpwelt/bloc/auth/auth_state.dart';
import 'package:immpwelt/bloc/home/home_bloc.dart';
import 'package:immpwelt/bloc/home/home_event.dart';
import 'package:immpwelt/bloc/home/home_state.dart';
import 'package:immpwelt/config.dart';
import 'package:immpwelt/constants.dart';
import 'package:immpwelt/models/City.dart';
import 'package:immpwelt/models/Place.dart';
import 'package:immpwelt/models/User.dart';
import 'package:immpwelt/services/AuthApi.dart';
import 'package:immpwelt/services/HomeApi.dart';
import 'package:immpwelt/widgets/MyAppBar.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'LoginPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> cities = ["damascus", "Homs"];
  List<City> cities2 = [];
  List<City> district = [];
  List<Place> pleaces = [];
  bool isRent = true;
  bool isPruches = false;
  String is_rent = "1";
  int cityId;
  int districtId = 1;
  int selectedCity;
  int selecteddistrict;
  final TextEditingController city = TextEditingController();
  final TextEditingController areafrom = TextEditingController();
  final TextEditingController areato = TextEditingController();
  final TextEditingController numberofpeoplef = TextEditingController();
  final TextEditingController numberofpeoplet = TextEditingController();
  final TextEditingController numberofroomsf = TextEditingController();
  final TextEditingController numberofroomst = TextEditingController();
  final TextEditingController numberofbathroomsf = TextEditingController();
  final TextEditingController numberobathfroomst = TextEditingController();
  final TextEditingController pricef = TextEditingController();
  final TextEditingController pricet = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AuthBloc authBloc;
  AuthApi authApi = AuthApi(httpClient: http.Client());
  String area;
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    districtId = null;
    district.clear();
    cities.clear();
    authBloc = BlocProvider.of<AuthBloc>(context);
    homeBloc = HomeBloc(homeApi: homeApi);
    authBloc.add(GetCities());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: myAppBar("HomePage", null),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              width: size.width,
              child: DrawerHeader(
                child: Image.asset(
                  'assets/icon.png',
                  fit: BoxFit.cover,
                ),
                decoration: BoxDecoration(),
              ),
            ),
            ISAuth
                ? SizedBox()
                : ListTile(
                    leading: Icon(
                      Icons.login,
                      color: Config.primaryColor,
                      size: 35,
                    ),
                    title: Text(
                      'Login',
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/login');
                    },
                  ),
            UserRole == 1 || ISAuth
                ? ListTile(
                    leading: Icon(
                      Icons.login,
                      color: Config.primaryColor,
                      size: 35,
                    ),
                    title: Text(
                      'Add a house',
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      Navigator.pushReplacementNamed(context, '/addPlace');
                    },
                  )
                : SizedBox(),
            UserRole == 1 && ISAuth
                ? ListTile(
                    leading: Icon(
                      Icons.place,
                      color: Config.primaryColor,
                      size: 35,
                    ),
                    title: Text(
                      'Home Request',
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/homeRequest');
                    },
                  )
                : SizedBox(),
            UserRole == 1
                ? ListTile(
                    leading: Icon(
                      Icons.home,
                      color: Config.primaryColor,
                      size: 35,
                    ),
                    title: Text(
                      'Places',
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      Get.to(AllPlaces());
                    },
                  )
                : SizedBox(),
            ISAuth
                ? ListTile(
                    leading: Icon(
                      Icons.logout,
                      color: Config.primaryColor,
                      size: 35,
                    ),
                    title: Text(
                      'Logout',
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      authBloc.add(LogoutRequested());
                      setState(() {});
                    },
                  )
                : SizedBox(),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              BlocBuilder(
                cubit: authBloc,
                builder: (context, state) {
                  if (state is AuthLoadInProgress ||
                      state is CitiesLoadSuccess ||
                      state is AuthInitial ||
                      state is DistrictsLoadSuccess) {
                    if (state is CitiesLoadSuccess) {
                      cities2 = state.cities;
                    }
                    if (state is DistrictsLoadSuccess) {
                      district = state.cities;
                    }
                    return Row(
                      children: [
                        Flexible(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey[400],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.0))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DropdownButton<int>(
                                iconSize: 26,
                                isExpanded: true,
                                underline: SizedBox(),
                                hint: Text("City"),
                                value: cityId,
                                onChanged: (newValue) {
                                  setState(() {
                                    cityId = newValue;
                                  });
                                },
                                items: cities2
                                    .map<DropdownMenuItem<int>>((e) =>
                                        DropdownMenuItem<int>(
                                          onTap: () {
                                            setState(() {
                                              district.clear();
                                              selectedCity = e.id;
                                              districtId = null;
                                            });
                                            authBloc.add(
                                                Getdistrict(id: selectedCity));
                                          },
                                          value: e.id,
                                          child: new Text(e.displayName,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: 14)),
                                        ))
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey[400],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.0))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DropdownButton<int>(
                                iconSize: 26,
                                isExpanded: true,
                                underline: SizedBox(),
                                hint: Text("district"),
                                value: districtId,
                                onChanged: (newValue) {
                                  setState(() {
                                    districtId = newValue;
                                  });
                                },
                                items: district
                                    .map<DropdownMenuItem<int>>((e) =>
                                        DropdownMenuItem<int>(
                                          onTap: () {
                                            selecteddistrict = e.id;
                                          },
                                          value: e.id,
                                          child: new Text(e.displayName,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: 14)),
                                        ))
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  }

                  return Container();
                },
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          isRent = true;
                          isPruches = false;
                          is_rent = "1";
                        });
                      },
                      child: Container(
                        width: size.width,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: isRent ? Config.primaryColor : Colors.white,
                            border: Border.all(
                                width: 1,
                                color: isRent
                                    ? Config.primaryColor
                                    : Colors.grey)),
                        child: Center(child: Text("Rent")),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          isRent = false;
                          isPruches = true;
                          is_rent = "0";
                        });
                      },
                      child: Container(
                        width: size.width,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color:
                                isPruches ? Config.primaryColor : Colors.white,
                            border: Border.all(
                                width: 1,
                                color: isPruches
                                    ? Config.primaryColor
                                    : Colors.grey)),
                        child: Center(child: Text("Purchase")),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              BlocListener(
                cubit: homeBloc,
                listener: (context, state) {
                  if (state is SearchLoadSuccess) {
                    pleaces = state.places;
                    Navigator.pushNamed(context, '/SearchResult', arguments: {
                      'pleaces': pleaces,
                    });
                  }
                  if (state is SearchIsEmpty) {
                    Get.snackbar('', 'No Places Found',
                        snackPosition: SnackPosition.TOP,
                        overlayBlur: 3,
                        dismissDirection: SnackDismissDirection.HORIZONTAL,
                        colorText: Config.primaryColor,
                        backgroundColor: Colors.black,
                        duration: Duration(seconds: 3));
                  }
                },
                child: BlocBuilder(
                  cubit: homeBloc,
                  builder: (context, state) {
                    if (state is HomeLoadInProgress) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    if (state is HomeInitial ||
                        state is SearchLoadSuccess ||
                        state is HomeLoadFailure) {
                      return Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Home Space",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: areafrom,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\m2",
                                              textAlign: TextAlign.left,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'From',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: areato,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\m2",
                                              textAlign: TextAlign.right,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'To',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of People",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofpeoplef,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.person,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'From',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofpeoplet,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.person,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'To',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of Rooms",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofroomsf,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bed,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'From',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofroomst,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bed,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'To',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Number of BathRoom",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberofbathroomsf,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bed,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'From',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: numberobathfroomst,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Icon(
                                              Icons.bathroom,
                                              textDirection: TextDirection.ltr,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'To',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Price",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: pricef,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\$",
                                              textAlign: TextAlign.right,
                                              textDirection: TextDirection.rtl,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'From',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Flexible(
                                  child: Container(
                                      height: 50,
                                      child: TextField(
                                        controller: pricet,
                                        keyboardType: TextInputType.number,
                                        decoration: new InputDecoration(
                                            suffix: Text(
                                              "\$",
                                              textAlign: TextAlign.right,
                                              textDirection: TextDirection.rtl,
                                            ),
                                            border: new OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: new BorderSide(
                                                    color: Colors.teal)),
                                            labelText: 'To',
                                            suffixStyle: const TextStyle(
                                                color: Colors.green)),
                                      )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            GestureDetector(
                              onTap: () {
                                if (areafrom.text.length == 0) {
                                  Get.snackbar(
                                      'Error', 'Please Enter the Space',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (areato.text.length == 0) {
                                  Get.snackbar('Error', 'Please Enter the Area',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (numberofpeoplef.text.length == 0) {
                                  Get.snackbar('Error',
                                      'Please Enter the Number of Person',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (numberofpeoplet.text.length == 0) {
                                  Get.snackbar('Error',
                                      'Please Enter the Number of Person',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (pricef.text.length == 0) {
                                  Get.snackbar(
                                      'Error', 'Please Enter the Price',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                if (pricet.text.length == 0) {
                                  Get.snackbar(
                                      'Error', 'Please Enter the Price',
                                      snackPosition: SnackPosition.BOTTOM,
                                      overlayBlur: 3,
                                      dismissDirection:
                                          SnackDismissDirection.HORIZONTAL,
                                      colorText: Config.primaryColor,
                                      backgroundColor: Colors.black,
                                      duration: Duration(seconds: 3));
                                  return;
                                }
                                homeBloc.add(Search(
                                  price_from: areafrom.text,
                                  price_to: areato.text,
                                  max_persons_from: numberofpeoplef.text,
                                  number_of_rooms_from: numberofroomsf.text,
                                  number_of_rooms_to: numberofroomst.text,
                                  number_of_bathrooms_from:
                                      numberofbathroomsf.text,
                                  number_of_bathrooms_to:
                                      numberobathfroomst.text,
                                  max_persons_to: numberofpeoplet.text,
                                  is_rent: is_rent,
                                  city: selectedCity.toString(),
                                  district: selecteddistrict.toString(),
                                ));
                                // Navigator.pushReplacementNamed(context, '/test');
                              },
                              child: Container(
                                width: size.width * 0.85,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  color: Config.primaryColor,
                                  borderRadius: BorderRadius.horizontal(
                                    left: Radius.circular(10),
                                    right: Radius.circular(10),
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Search",
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
