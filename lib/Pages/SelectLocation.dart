import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:immpwelt/Pages/Map.dart';
import 'package:immpwelt/widgets/MyAppBar.dart';

import '../Config.dart';

class SelectLocation extends StatefulWidget {
  @override
  _SelectLocationState createState() => _SelectLocationState();
}

class _SelectLocationState extends State<SelectLocation> {
  StreamSubscription _mapIdleSubscription;
  GoogleMapController _mapController;
  Set<Marker> markers = Set<Marker>();
  Position myPosition;
  @override
  void initState() {
    super.initState();
    _determinePosition();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // serviceEnabled = await Geolocator.isLocationServiceEnabled();
    // if (!serviceEnabled) {
    //   return Future.error('Location services are disabled.');
    // }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    Position p = await Geolocator.getCurrentPosition();

    setState(() {
      myPosition = p;
    });

    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            myPosition.latitude - 0.0001,
            myPosition.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            myPosition.latitude,
            myPosition.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      appBar: myAppBar("Select Location", null),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            height: sizeAware.height,
            width: sizeAware.width,
            child: GoogleMap(
              onTap: (argument) {
                _mapTapped(argument, setState);
              },
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              initialCameraPosition: CameraPosition(
                target: LatLng(33.3037443, 36.6823914),
                zoom: 10,
              ),
              markers: markers,
              onMapCreated: (mapController) {
                _mapController = mapController;
              },
              onCameraMove: (newPosition) {
                _mapIdleSubscription?.cancel();
                _mapIdleSubscription =
                    Future.delayed(Duration(milliseconds: 150))
                        .asStream()
                        .listen((_) {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: sizeAware.width * 0.5,
                    height: 60,
                    child: RaisedButton(
                      color: Config.primaryColor,
                      child: Text(
                        'Select',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      onPressed: () {
                        if (markers.length > 0) {
                          Navigator.pop(
                            context,
                            {'latlang': markers.first.position},
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _mapTapped(LatLng location, state) {
    PointObject point2 = PointObject(
      child: GestureDetector(
        onTap: () {},
        child: Text('Address'),
      ),
      location: LatLng(location.latitude, location.longitude),
    );

    setState(() {
      state(() {
        markers.clear();
        markers.add(
          Marker(
            markerId: MarkerId(point2.location.latitude.toString() +
                point2.location.longitude.toString()),
            position: point2.location,
            onTap: () => _onTap(point2),
          ),
        );
      });
    });
// The result will be the location you've been selected
// something like this LatLng(12.12323,34.12312)
// you can do whatever you do with it
  }

  _onTap(PointObject point) async {
    final RenderBox renderBox = context.findRenderObject();
    Rect _itemRect = renderBox.localToGlobal(Offset.zero) & renderBox.size;

    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude - 0.0001,
            point.location.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude,
            point.location.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
  }
}

class PointObject {
  final Widget child;
  final LatLng location;

  PointObject({this.child, this.location});
}
