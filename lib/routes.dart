import 'package:flutter/material.dart';
import 'package:immpwelt/Pages/AddPlace.dart';
import 'package:immpwelt/Pages/HomePage.dart';
import 'package:immpwelt/Pages/LoginPage.dart';
import 'package:immpwelt/Pages/Map.dart';
import 'package:immpwelt/Pages/RegisterPage.dart';
import 'package:immpwelt/Pages/AllPlacesPage.dart';
import 'package:immpwelt/Pages/RequestPlaces.dart';
import 'package:immpwelt/Pages/SearchPage.dart';
import 'package:immpwelt/Pages/SinglePlace.dart';
import 'package:immpwelt/Pages/SplashPage.dart';
import 'package:immpwelt/Pages/UpdatePlace.dart';

class Routes {
  static Map<String, WidgetBuilder> getAll() {
    return {
      "/home": (context) => HomePage(),
      "/login": (context) => LoginPage(),
      "/register": (context) => RegisterPage(),
      "/test": (context) => AllPlaces(),
      "/SearchResult": (context) => SearchResult(),
      "/SinglePlace": (context) => SinglePlace(),
      "/map": (context) => MyMap(),
      "/update": (context) => UpdatePlace(),
      "/splash": (context) => SplashPage(),
      "/addPlace": (context) => AddPlace(),
      "/homeRequest": (context) => RequestPlaces(),
    };
  }
}
